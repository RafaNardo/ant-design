import './App.less'
import { BrowserRouter as Router } from 'react-router-dom';
import { Layout } from 'antd';
import { Header, Menu } from './components/layouts'
import { useState } from 'react';
import Routes from './routes';

const { Content } = Layout;

const App = function () {
  const [collapsed, setCollapsed] = useState(false);
  const [hiddenSiderTriggerButton, setHidenSiderTriggerButton] = useState(true);

  const onBreakpoint = function (broken: boolean) {
    if (broken) {
      setCollapsed(true);
      setHidenSiderTriggerButton(false);
    }
    else {
      setCollapsed(false);
      setHidenSiderTriggerButton(true);
    }
  }
  
  return (
    <Router>
        <Layout style={ { height: "100%" } }>
          <Header { ...{ collapsed, setCollapsed, hiddenSiderTriggerButton } } />
          <Layout>
            <Menu  { ...{ collapsed, onBreakpoint } }/>
            <Content className="page" >
              <Routes />
            </Content>
          </Layout>
        </Layout>
    </Router>
  );
}

export default App;
