import './index.less'
import iplaceLogo from './images/iplace.png';
import taqiLogo from './images/taqi.png';
import hervalLogo from './images/iplace.png';
import { ENegocio } from '../../../../../../../types/enums/e-negocio';

interface LogoImageProps {
    negocio: ENegocio;
}

const getLogoImage = function (negocio: ENegocio) {
    switch (negocio) {
        case ENegocio.Iplace:
            return iplaceLogo;
        case ENegocio.Taqi:
            return taqiLogo;
        case ENegocio.Herval:
        default:
            return hervalLogo;
    }
}

const LogoImage: React.FC<LogoImageProps> = function ({negocio}) {
    return (
        <img src={getLogoImage(negocio)} alt={`Logo - ${negocio.toString()}`}/>
    )
}

export default LogoImage;