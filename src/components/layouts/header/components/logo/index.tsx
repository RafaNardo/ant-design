import './index.less'
import { ENegocio } from '../../../../../types/enums/e-negocio';
import LogoImage from './components/logo-image'

interface LogoProps {
    siteName: string;
    negocio: ENegocio;
}

const Logo: React.FC<LogoProps> = function ({siteName, negocio}) {
    return (
        <div className="logo">
          <h1>
            <a href="/">
              <LogoImage negocio={negocio}/>
              {siteName}
            </a>
          </h1>
        </div>
    )
}

export default Logo;