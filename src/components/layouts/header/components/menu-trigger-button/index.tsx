import './index.less'
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';

interface SiderTriggerButtonProps {
  collapsed: boolean,
  hidden: boolean,
  setCollapsed: React.Dispatch<React.SetStateAction<boolean>>
}

const MenuTriggerButton: React.FC<SiderTriggerButtonProps> = function ({hidden, collapsed, setCollapsed}) {
  return (
    <div className="trigger-buttons" hidden={hidden}>
      <MenuFoldOutlined className="trigger" hidden={collapsed} onClick={() => setCollapsed(true)} />
      <MenuUnfoldOutlined className="trigger" hidden={!collapsed} onClick={() => setCollapsed(false)} />
    </div>
  )
}

export default  MenuTriggerButton;