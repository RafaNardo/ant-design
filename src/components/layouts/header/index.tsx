import './index.less';
import { UserOutlined } from '@ant-design/icons';
import Avatar from 'antd/lib/avatar/avatar';
import { Layout } from 'antd';
import Logo from './components/logo';
import { ENegocio } from '../../../types/enums/e-negocio';
import MenuTriggerButton from './components/menu-trigger-button';

const AntdHeader = Layout.Header;

interface HeaderProps {
  collapsed: boolean,
  setCollapsed: React.Dispatch<React.SetStateAction<boolean>>
  hiddenSiderTriggerButton: boolean
}

const Header: React.FC<HeaderProps> = ({collapsed, setCollapsed, hiddenSiderTriggerButton}) => {
    return (
        <AntdHeader className="header" style={{ padding: 0 }}>
        <div className="left-container">
          <Logo siteName="Corporativo Iplace" negocio={ENegocio.Iplace} />
          <MenuTriggerButton collapsed={collapsed} setCollapsed={setCollapsed} hidden={hiddenSiderTriggerButton} />
        </div>
        <div className="right-container">
          <Avatar size="large" icon={<UserOutlined />} />
        </div>
      </AntdHeader>
    )
}

export default Header;