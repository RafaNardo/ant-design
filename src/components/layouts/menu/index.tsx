import "./index.less";
import { Menu as AntdMenu } from "antd";
import { HomeOutlined, AppstoreOutlined, MailOutlined } from '@ant-design/icons';
import { NavLink } from "react-router-dom";
import { Layout } from 'antd';

const { SubMenu } = AntdMenu;

const { Sider } = Layout;

interface MenuProps {
    collapsed: boolean,
    onBreakpoint: any
}

const Menu: React.FC<MenuProps> = ({collapsed, onBreakpoint}) =>  {
    const getDefaultPath = () => {
        const paths = window.location.pathname.split('/')
        if (paths.length === 1)
            return paths[0]
        else
            return `/${paths[1]}`
    }

    return (
        <Sider trigger={null} collapsible collapsed={collapsed} collapsedWidth={0} breakpoint="lg" onBreakpoint={onBreakpoint}>
            <AntdMenu theme="dark" mode="inline" defaultSelectedKeys={ [ getDefaultPath() ] } >
                <AntdMenu.Item key="/" icon={<HomeOutlined />}>
                    <NavLink to="/">Home</NavLink>
                </AntdMenu.Item>
                <AntdMenu.Item key="/tipos-de-cotacoes" icon={<AppstoreOutlined />}>
                    <NavLink to="/tipos-de-cotacoes">Tipos de Cotações</NavLink>
                </AntdMenu.Item>
                <SubMenu key="sub1" icon={<MailOutlined />} title="Exemplos">
                    <AntdMenu.Item key="/exemplos/tiago">
                        <NavLink to="/exemplos/tiago">Tiago</NavLink>
                    </AntdMenu.Item>
                    <AntdMenu.Item key="/exemplos/rodrigo">
                        <NavLink to="/exemplos/rodrigo">Rodrigo</NavLink>
                    </AntdMenu.Item>
                    <AntdMenu.Item key="/exemplos/marcelo">
                        <NavLink to="/exemplos/marcelo">Marcelo</NavLink>
                        </AntdMenu.Item>
                    <AntdMenu.Item key="/exemplos/deivid">
                        <NavLink to="/exemplos/deivid">Deivid</NavLink>
                    </AntdMenu.Item>
                </SubMenu>
            </AntdMenu >
        </Sider>
    );
}

export default Menu