import { Route } from 'react-router-dom';
import { PageHeader } from 'antd';
import Pages  from '../pages';
import './index.less'

const ROUTES = {
    HOME: {
        component: Pages.Home,
        exact: true,
        path: "/",
        title: "Home",
    },
    TIPOS_DE_COTACOES: {
        component: Pages.TiposDeCotacoes.Listagem,
        exact: true,
        path: "/tipos-de-cotacoes",
        title: "Tipos de cotações",
    },
    TIPOS_DE_COTACOES_EDICAO: {
        component: Pages.TiposDeCotacoes.Edicao,
        exact: true,
        path: "/tipos-de-cotacoes/:id",
        title: "Tipos de cotações"
    },
    EXEMPLO_DEIVID: {
        component: Pages.Examples.ExampleDeivid,
        exact: true,
        path: "/exemplos/deivid",
        title: "Exemplos Deivid"
    },
    EXEMPLO_TIAGO: {
        component: Pages.Examples.ExampleTiago,
        exact: true,
        path: "/exemplos/tiago",
        title: "Exemplos Tiago"
    },
    EXEMPLO_RODRIGO: {
        component: Pages.Examples.ExampleRodrigo,
        exact: true,
        path: "/exemplos/rodrigo",
        title: "Exemplos Rodrigo"
    },
    EXEMPLO_MARCELO: {
        component: Pages.Examples.ExampleMarcelo,
        exact: true,
        path: "/exemplos/marcelo",
        title: "Exemplos Marcelo"
    }
};

const renderComponent = (route: any) => <>
    <PageHeader title={route.title} className="page-header" />
    { route.component()}
</>

const Routes = () => <>
    {Object.values(ROUTES).map((route, index) =>
        <Route key={index} exact={route.exact} path={route.path} component={() => renderComponent(route)}></Route>
    )}
</>

export default Routes;