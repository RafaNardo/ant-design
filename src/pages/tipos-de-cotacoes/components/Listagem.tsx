import React from 'react';
import {
    Button,
    Table,
    Tag,
} from 'antd';
import Column from 'antd/lib/table/Column';
import { Link, useRouteMatch } from 'react-router-dom';

const Listagem = function () {
    let match = useRouteMatch();

    const renderTagSituacao = (situacao: number) => {
        return situacao === 1 ? <Tag color="warning">Bloqueado</Tag> : <Tag color="success">Liberado</Tag>
    }

    const renderNovoButton = () =>
        <Button type="primary">
            <Link to="/Novo">
                Novo
            </Link>
        </Button>

    const renderDetalhesButton = (tipoCondicao: any) =>
        <Button type="primary">
            <Link to={`${match.url}/${tipoCondicao.id}`}>
                Detalhes
            </Link>
        </Button>

    const dataSource = [
        {
            id: 1,
            nome: 'Com acessórios Iphone',
            comissaoInterna: 1.00,
            comissaoExterna: 2.00,
            situacao: 1,
        },
        {
            id: 2,
            nome: 'Com acessórios Ipad',
            comissaoInterna: 1.50,
            comissaoExterna: 2.44,
            situacao: 2,
        },
    ];

    return (
        <Table dataSource={dataSource} rowKey="Id">
            <Column title="Id" dataIndex="id" key="id"></Column>
            <Column title="Nome" dataIndex="nome" key="nome"></Column>
            <Column responsive={['md']} title="Comissao Interna" dataIndex="comissaoInterna" key="comissaoInterna" render={comissao => `${comissao}%`}></Column>
            <Column responsive={['md']} title="Comissao Externa" dataIndex="comissaoExterna" key="comissaoExterna" render={comissao => `${comissao}%`}></Column>
            <Column responsive={['lg']} title="Situacao" dataIndex="situacao" key="situacao" render={renderTagSituacao}></Column>
            <Column align="right" render={renderDetalhesButton} title={renderNovoButton()}></Column>
        </Table>
    );
};

export default Listagem;