import React, { useState } from 'react';
import {
    Form,
    Input,
    Button
} from 'antd';

const Edicao = function () {

    const [tipoCotacao] = useState({
        id: '1',
        nome: 'Com acessórios Iphone',
        comissaoInterna: 1.00,
        comissaoExterna: 2.00,
        situacao: 1,
    });

    const onFinish = (values: any) => {
        console.log('Success:', values);
    };

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <Form
            labelCol={{ span: 3}}
            wrapperCol={{ span: 8 }}
            name="detalhes"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
        >
            <Form.Item
                label="Id"
                name="id"
            >
                <Input readOnly={true} value={tipoCotacao.id} />
            </Form.Item>

            <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ span: 11 }} style={{ textAlign: "right" }}>
                <Button type="primary" htmlType="submit" >
                    Salvar
                </Button>
            </Form.Item>
        </Form>
    );
};

export default Edicao;