import Edicao from "./components/Edicao";
import Listagem from "./components/Listagem";

const TiposDeCotacoes = {
    Edicao,
    Listagem
}

export default TiposDeCotacoes