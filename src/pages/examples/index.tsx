import ExampleDeivid from './components/deivid'
import ExampleRodrigo from './components/rodrigo'
import ExampleTiago from './components/tiago'
import ExampleMarcelo from './components/marcelo'

const Examples = {
    ExampleDeivid,
    ExampleRodrigo,
    ExampleTiago,
    ExampleMarcelo
}

export default Examples