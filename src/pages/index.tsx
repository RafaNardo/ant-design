import Home from './home'
import TiposDeCotacoes from './tipos-de-cotacoes'
import Examples from './examples'

const Pages = {
    Home,
    TiposDeCotacoes,
    Examples
}

export default Pages;